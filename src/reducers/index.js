import { combineReducers } from 'redux'
import { RECEIVE_STORIES } from '../actions'
import { SORT_CHANGED } from '../actions/sort'
import sortStories from '../helpers/sortStories'


const stories = (state = [], action) => {
  switch (action.type) {
    case RECEIVE_STORIES:
      return [...action.stories]

    case SORT_CHANGED:
      const sorted = sortStories(state, action.value)
      return [...sorted]

    default:
      return state
  }
}


export default combineReducers({
  stories,
})
