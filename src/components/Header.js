import React, { PureComponent } from 'react'
import { Link } from 'react-router'
import PropTypes from 'prop-types'


export default class Header extends PureComponent {
  static propTypes = {
    onFilterToggle: PropTypes.func,
    title: PropTypes.string,
  }

  static defaultProps = {
    title: "Zonky",
  }

  render() {
    const { title } = this.props

    return (
      <header>
        <div className="container">
          <div className="row">
            <div className="col">
              <h1 className="logo">
                <Link to="/">{title}</Link>
              </h1>
            </div>
          </div>
        </div>
      </header>
    )
  }
}
