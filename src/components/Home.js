import React, { PureComponent } from 'react'
// import PropTypes from 'prop-types'
import { Container, Row, Col } from 'reactstrap'
import { Story } from '../components'
import { DropdownContainer } from '../containers'
import truncate from '../helpers/truncate'


export default class Home extends PureComponent {
  static defaultProps = {
    stories: [],
  }

  render() {
    const { stories } = this.props

    return (
      <div className="home">
        <Container>
          <Row>
            <Col>
              <h2>Tržište</h2>
            </Col>
            <Col className="sort">
              <DropdownContainer
                options={[
                  {title: 'Řadit podle', value: 'default'},
                  {title: 'Délky trvání', value: 'termInMonths'},
                  {title: 'Ratingu', value: 'rating'},
                  {title: 'Požadované částky', value: 'amount'},
                  {title: 'Deadline', value: 'deadline'},
                ]}
              />
            </Col>
          </Row>
          <Row className="stories-wrap">
            {stories.map((story, index) =>
              <Story
                key={index} 
                url={`/${story.id}`}
                imgUrl={`https://api.zonky.cz${story.photos[0].url}`}
                title={story.name}
                text={truncate(story.story, 100)}
                goalAmmount={story.amount}
                rating={story.rating}
              />
            )}
          </Row>
        </Container>
      </div>
    )
  }
}
