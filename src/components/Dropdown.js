import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'


export default class Dropdown extends PureComponent {
  static propTypes = {
    options: PropTypes.arrayOf(
      PropTypes.shape({
        title: PropTypes.string,
        value: PropTypes.string.isRequired,
      })
    ),
    onChange: PropTypes.func.isRequired,
  }

  render() {
    const { options, selected, onChange } = this.props

    return (
      <select 
        className="form-control"
        onChange={onChange}
        defaultValue={selected}
      >
        {options.map((option, index) => 
          <option 
            key={index}
            value={option.value}
          >
            {option.title}
          </option> 
        )}
      </select>
    )
  }
}
