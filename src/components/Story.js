import React, { PureComponent } from 'react'
import { Col } from 'reactstrap'
import { Link } from 'react-router'
import PropTypes from 'prop-types'


export default class Story extends PureComponent {
  static propTypes = {
    url: PropTypes.string.isRequired,
    imgUrl: PropTypes.string.isRequired,
    goalAmmount: PropTypes.number.isRequired,
    rating: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
  }

  render() {
    const { url, imgUrl, title, text, goalAmmount, rating } = this.props

    return (
      <Col md={6}>
        <Link to={url} className="story">
          
          <div className="img-cont">
            <img src={imgUrl} alt=""/>
            <div className="mobile">
              <p className="ammount">{goalAmmount}</p>
              <p className="rating">{rating}</p>
            </div>
          </div>

          <div className="right">
            <h3>{title}</h3>
            <div className="desktop">
              <p className="ammount">
                {goalAmmount}
                <span className="rating">
                  {rating}
                </span>
              </p>
            </div>
            <p className="text">{text}</p>
          </div>

        </Link>
      </Col>
    )
  }
}
