import React, { PureComponent } from 'react';
import PropTypes from 'prop-types'
import { Container, Row, Col } from 'reactstrap'
import ProgressBar from './ProgressBar'


export default class StoryDetail extends PureComponent {
  static propTypes = {
    title: PropTypes.string,
    imgUrl: PropTypes.string,
    rating: PropTypes.string,
    amount: PropTypes.number,
    currentAmount: PropTypes.number,
    deadline: PropTypes.instanceOf(Date),
    interestRate: PropTypes.number,
    termInMonths: PropTypes.number,
    investmentsCount: PropTypes.number,
    user: PropTypes.shape({
      name: PropTypes.string,
      region: PropTypes.string,
      income: PropTypes.string,
      text: PropTypes.string,
    })
  }

  static defaultProps = {
    user: {}
  }

  render() {
    const { title, rating, currentAmount, amount, imgUrl, termInMonths, user, interestRate, investmentsCount } = this.props
    const completedInPercent = Math.round(currentAmount / amount * 1000) / 10

    return (
      <div className="story-detail">

        <div className="head">
          <Container>
            <Row>
              <Col xs="12" md="4" className="photo-wrap">
                <img src={imgUrl} alt="" />
              </Col>
              <Col xs="12" md="8">
                
                <Row>
                  <Col xs={9}>
                    <h2>{title}</h2>
                  </Col>
                  <Col xs={3} className="right">
                    {rating}
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <ProgressBar
                      progress={completedInPercent}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <span>{`${completedInPercent}% investováno`}</span>
                  </Col>
                  <Col className="right">
                    <span>{amount} CZK</span>
                  </Col>
                </Row>
                <div className="info-panel">
                  <Row>
                    <Col>
                      <p className="remaining">
                        {`Zbýva 27 dní a ${amount - currentAmount} CZK`}
                      </p>
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <p className="label">Délka</p>
                      <p className="value">{termInMonths} měsíců</p>
                    </Col>
                    <Col>
                      <p className="label">Jméno</p>
                      <p className="value">{user.name}</p>
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <p className="label">Úrok</p>
                      <p className="value">{interestRate * 100}%</p>
                    </Col>
                    <Col>
                      <p className="label">Kraj</p>
                      <p className="value">{user.region}</p>
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <p className="label">Investovalo</p>
                      <p className="value">{investmentsCount}</p>
                    </Col>
                    <Col>
                      <p className="label">Typ príjmu</p>
                      <p className="value">{user.income}</p>
                    </Col>
                  </Row>
                </div>

                <div className="text-panel">
                  <Row>
                    <Col>
                      <p>{user.text}</p>
                    </Col>
                  </Row>
                </div>

              </Col>
              
            </Row>
          </Container>
        </div>
      </div>
    )
  }
}
