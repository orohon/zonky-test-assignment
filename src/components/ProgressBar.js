import React, { PureComponent } from 'react'


export default class ProgressBar extends PureComponent {
  render() {
    const { progress } = this.props

    return (
      <div className="progress-bar">
        <div 
          className="progress-stripe" 
          style={{
            width: `${progress}%`
          }}
        />
      </div>
    )
  }
}
