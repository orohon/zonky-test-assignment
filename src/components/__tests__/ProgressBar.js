import React from 'react'
import ProgressBar from '../ProgressBar'
import renderer from 'react-test-renderer'


it('ProgressBar should match snapshot', () => {
  
  const tree = renderer.create(
    <ProgressBar progress={50} />
  ).toJSON()

  expect(tree).toMatchSnapshot()

})

