import React from 'react'
import StoryDetail from '../StoryDetail'
import renderer from 'react-test-renderer'


it('StoryDetail should match snapshot', () => {
  
  const tree = renderer.create(
   <StoryDetail
      url="/some-url" 
      title="Story Title"
      rating="A++"
      currentAmount={5000}
      amount={10000}
      imgUrl="https://www.some.com/image.png"
      termInMonths={48}
      deadline={new Date()}
      user={{
        name: 'Jozef Investor',
        region: '1',
        income: 'employment',
        text: 'Some text',
      }}
      interestRate={0.05}
      investmentsCount={100}
    />
  ).toJSON()

  expect(tree).toMatchSnapshot()

})

