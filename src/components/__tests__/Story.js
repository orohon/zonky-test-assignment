import React from 'react'
import Story from '../Story'
import renderer from 'react-test-renderer'


it('Story should match snapshot', () => {
  
  const tree = renderer.create(
    <Story 
      url="/some-link"
      imgUrl="https://api.zonky.cz/loans/105537/photos/11620"
      title="Auto do zaměstnání"
      text="Dobrý den. Můj původní plechový miláček už odešel a proto jsem byl nucen využít velmi nevýhodné financování vozu. Půjčku bych využil na refinancování této půjčky na vozidle Toyota Corolla (r.v. 2006). Všem případným investorům předem děkuji. Předpokládám dřívější splacení půjčky."
      goalAmmount={80000}
      rating="A++"
    />
  ).toJSON()

  expect(tree).toMatchSnapshot()

})

