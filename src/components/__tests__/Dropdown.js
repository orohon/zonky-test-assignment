import React from 'react'
import Dropdown from '../Dropdown'
import renderer from 'react-test-renderer'


it('Dropdown should match snapshot', () => {
  
  const tree = renderer.create(
    <Dropdown 
      options={[{value: 'val', text: 'text'}, {value: 'val2', text: 'text2'}]}
      onChange={() => console.log('dropdown')}
    />
  ).toJSON()

  expect(tree).toMatchSnapshot()

})

