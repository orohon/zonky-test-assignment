// import axios from 'axios'
// import { API_URL } from '../constants'
import fakeResponse from '../fakeResponse'


export const REQUEST_STORIES = 'REQUEST_STORIES'
export const RECEIVE_STORIES = 'RECEIVE_STORIES'
export const REQUEST_STORIES_FAILED = 'REQUEST_STORIES_FAILED'


export const requestStories = () => {
  return {
    type: REQUEST_STORIES,
  }
}

export const receiveStories = (json) => {
  return {
    type: RECEIVE_STORIES,
    stories: json,
    receivedAt: Date.now(),
  }
}

export const requestStoriesFail = (error) => {
  return {
    type: REQUEST_STORIES_FAILED,
    error,
  }
}

export const fetchStories = () => {
  return dispatch => {
    
    dispatch(requestStories)
    
    // return axios
    //   .get(API_URL)
    //   .then(response => dispatch( receiveStories(response) ))
    //   .catch(error => dispatch( requestStoriesFail(error) ))

    setTimeout(() => {
      dispatch(receiveStories(fakeResponse))
    }, 300)

  }
}
