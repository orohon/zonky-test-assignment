export const SORT_CHANGED = 'SORT_CHANGED'

export const changeSort = (value) => {
  return {
    type: SORT_CHANGED,
    value,
  }
}
