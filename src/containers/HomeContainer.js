import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { fetchStories } from '../actions'
import { Home } from '../components'


class HomeContainer extends PureComponent {
  render() {
    return (
      <Home {...this.props} />
    )
  }

  componentDidMount() {
    this.props.onFetchRequested()
  }

}

const mapStateToProps = (state) => {
  return {
    stories: state.stories,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onFetchRequested: () => dispatch( fetchStories() ),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeContainer)
