import { connect } from 'react-redux'
import Dropdown from '../components/Dropdown'
import { changeSort } from '../actions/sort'


const mapDispatchToProps = (dispatch) => {
  return {
    onChange: e => dispatch(changeSort(e.target.value))
  }
}

const mapStateToProps = (state) => {
  return {
    selected: state.sort
  }
}


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Dropdown)
