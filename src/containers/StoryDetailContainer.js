import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { fetchStories } from '../actions'
import { StoryDetail } from '../components'


class StoryDetailContainer extends PureComponent {
  render() {
    return (
      <StoryDetail {...this.props} />
    )
  }

  componentDidMount() {
    this.props.onFetchRequested()
  }

}


const mapDispatchToProps = (dispatch) => {
  return {
    onFetchRequested: () => dispatch( fetchStories() ),
  }
}

const mapStateToProps = (state, ownProps) => {

  const story = state.stories.filter(story => {
    return story.id === parseInt(ownProps.params.id, 10)
  })[0]

  if (!story) return {}

  const { name, photos, rating, amount, remainingInvestment, deadline, interestRate, termInMonths, investmentsCount, nickName, region, mainIncomeType } = story

  return {
    title: name,
    imgUrl: `https://api.zonky.cz${photos[0].url}`,
    rating,
    amount,
    currentAmount: amount - remainingInvestment,
    deadline: new Date( Date.parse(deadline) ),
    interestRate,
    termInMonths,
    investmentsCount,
    user: {
      name: nickName,
      region,
      income: mainIncomeType,
      text: story.story,
    }
  }
}


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StoryDetailContainer)
