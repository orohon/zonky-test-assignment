import React from 'react'
import { Router, Route, IndexRoute, browserHistory } from 'react-router'
import { applyMiddleware, createStore } from 'redux'
import { Provider } from 'react-redux'
import thunkMiddleware from 'redux-thunk'
import logger from 'redux-logger'
import App from './App'
import { HomeContainer, StoryDetailContainer } from './containers'
import reducers from './reducers'


const store = createStore(
  reducers,
  applyMiddleware(
    thunkMiddleware,
    logger,
  )
)

const Root = () => (
  <Provider store={store}>
    <Router history={browserHistory}>
      <Route path="/" component={App}>
        <IndexRoute component={HomeContainer} />
        <Route path=":id" component={StoryDetailContainer} />
      </Route>
    </Router>
  </Provider>
)


export default Root
