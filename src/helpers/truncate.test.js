import truncate from './truncate'

it('truncate shortens longer string', () => {
  const string = truncate('1234567890', 5)
  expect(string).toBe('12...')
})

it('does not truncate shorter string', () => {
  const string = truncate('1234567890', 10)
  expect(string).toBe('1234567890')
})
