import sortStories from './sortStories'


const s1 = {
  "termInMonths": 12,
  "rating": "AAA",
  "amount": 415000,
  "deadline": "2017-07-13T15:11:23.586+02:00",
}

const s2 = {
  "termInMonths": 66,
  "rating": "AAAA",
  "amount": 200000,
  "deadline": "2017-07-13T15:16:15.128+02:00",
}

const s3 = {
  "termInMonths": 84,
  "rating": "AAA",
  "amount": 150000,
  "deadline": "2017-07-13T15:25:39.942+02:00",
}

const stories = [s1, s2, s3]


it('should sort by termInMonths', () => {
  const sorted = sortStories(stories, 'termInMonths')
  expect(sorted).toEqual([s1, s2, s3])
})

it('should sort by rating', () => {
  const sorted = sortStories(stories, 'rating')
  expect(sorted).toEqual([s2, s1, s3])
})

it('should sort by amount', () => {
  const sorted = sortStories(stories, 'amount')
  expect(sorted).toEqual([s3, s2, s1])
})

it('should sort by deadline', () => {
  const sorted = sortStories(stories, 'deadline')
  expect(sorted).toEqual([s1, s2, s3])
})
