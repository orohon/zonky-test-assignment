const ratingValue = {
  'AAAA': 0,
  'AAA': 1,
  'AA': 2,
  'A': 3,
  'B': 4,
  'C': 5,
  'D': 6,
}

const compareByNumber = (a, b) => {
  if (a < b) {
    return -1
  }
  if (a > b) {
    return 1
  }
  return 0
}

const compareByTerm = (a, b) => {
  return compareByNumber(a.termInMonths, b.termInMonths)
}

const compareByAmount = (a, b) => {
  return compareByNumber(a.amount, b.amount)
}

const compareByRating = (a, b) => {
  const aVal = ratingValue[a.rating]
  const bVal = ratingValue[b.rating]
  return compareByNumber(aVal, bVal)
}

const compareByDeadline = (a, b) => {
  const dateA = Date.parse(a)
  const dateB = Date.parse(b)
  return compareByNumber(dateA, dateB)
}


export default (stories, sort) => {
  switch (sort) {
    case 'rating':
      return stories.sort(compareByRating)

    case 'termInMonths':
      return stories.sort(compareByTerm)

    case 'amount':
      return stories.sort(compareByAmount)
    
    case 'deadline':
      return stories.sort(compareByDeadline).reverse()

    default:
      return stories
  }
}
