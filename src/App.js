import React, { Component } from 'react'
import { Header } from './components'


class App extends Component {
  render() {
    return (
      <div className="App">
        <Header 
          onFilterToggle={(e) => {
            e.preventDefault()
            console.log('filter toggle')
          }}
        />

        {this.props.children}
        
      </div>
    )
  }
}

export default App
